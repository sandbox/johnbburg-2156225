<?php


/**
 * Display the channels of an EE database
 */
function ee_analyzer_channels() {
  $query = ee_analyzer_get_connection();
  
  if($query){
    $channel_data = ee_analyzer_get_channel_data($query); 
  }
  else{
    return t('No active database connection');
  }
  
  $header = array(
    array(
      'data' => t('Channel ID'),
      'field' => 'channel_id',
    ),
    array(
      'data' => t('Channel Name'),
      'field' => 'channel_title',
    ),
    array(
      'data' => t('Total Entries'),
      'field' => 'total_entries',
    ),
   );
  
  $rows = array();
  
  foreach ($channel_data as $channel) {
    $rows[$channel->channel_id] = array(
      'channel_id' => $channel->channel_id,
      'channel_title' => l($channel->channel_title, 'admin/content/ee_analyzer/channels/'.$channel->channel_id),
      'total_entries' => $channel->total_entries,
    );
    
  }
  
  $render = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  return drupal_render($render); 
}


<?php


/**
 * Get data for a single channel
 */
function ee_analyzer_single_channel_data($channel_id) {

  if(!is_numeric($channel_id)){
    drupal_set_message('Invalid request', 'error');
    return '';
  } 

  return  ee_analyzer_get_single_channel_data_header($channel_id) 
        . ee_analyzer_single_channel_data_fields($channel_id); 
     
}


/**
 * Query data for a specific channel
 */
function ee_analyzer_get_single_channel_data($query, $channel_id) {
  
  $query = $query->select('exp_channels', 'ec');
  $query->fields('ec', array('channel_id', 'channel_title', 'total_entries'));
  $query->fields('ecf', array('group_id', 'field_id', 'field_name', 'field_label', 'field_type'));
  $query->innerJoin('exp_channel_fields', 'ecf', 'ec.field_group = ecf.group_id');
  $query->condition('ec.channel_id', $channel_id, '=');
  $result = $query->execute()
    ->fetchAllAssoc('field_id');
    
  return $result;
}

/**
 * Render a header for a channel detail page.
 */
function ee_analyzer_get_single_channel_data_header($channel_id) {
  $query = ee_analyzer_get_connection();
  
  if($query){
    $channel_data = ee_analyzer_get_channel_data($query, $channel_id);
    $channel_data = $channel_data->fetchAssoc();
  }
  
  drupal_set_title('Channel: ' . $channel_data['channel_title']);
  
  $meta = '<div><p>ID: ' . $channel_data['channel_id'] . '</p>';
  $meta .= '<p>Entries: ' . $channel_data['total_entries'] . '</p></div>';
  
  return $meta;
}

/**
 * Query and render the table of fields for a given channel
 */
function ee_analyzer_single_channel_data_fields($channel_id) {
  $query = ee_analyzer_get_connection();
  
  if($query){
    $fields = ee_analyzer_get_single_channel_data($query, $channel_id); 
  }
  else{
    return t('No active database connection');
  }
  
  $header = array(
    array(
      'data' => t('Field ID'),
      'field' => 'field_id',
    ),
    array(
      'data' => t('Field name'),
      'field' => 'field_name',
    ),
    array(
      'data' => t('Field label'),
      'field' => 'field_label',
    ),
    array(
      'data' => t('Field type'),
      'field' => 'field_type',
    ),
  );
  
  $rows = array();
  
  foreach ($fields as $field) {
    
    if($field->field_type == 'matrix'){
      $field->field_type = ee_analyzer_single_channel_matrix_field_link($field, $channel_id);
    }
    
    $rows[$field->field_id] = array(
      'field_id' => $field->field_id,
      'field_name' => $field->field_name, 
      'field_label' => $field->field_label,
      'field_type' => $field->field_type,
    );
  }
  
  $render = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  
  $table = drupal_render($render); 
  
  $entries = ee_analyzer_single_channel_entry_list($channel_id, $fields);
  return $table . $entries;
}

/**
 * Render a link to information for a matrix field. 
 */
function ee_analyzer_single_channel_matrix_field_link($field, $channel_id) {
  return l($field->field_type, 'admin/content/ee_analyzer/channels/' . $channel_id . '/matrix/' .$field->field_id );
}



/**
 * Render a channel list
 */
function ee_analyzer_single_channel_entry_list($channel_id, $fields){
  
  if(isset($_GET['page'])){
    $page = filter_xss($_GET['page']);
  }else{
    $page = 0;
  }
  if(!is_numeric($page)){
    $page = 0; 
  }
  
  $query = ee_analyzer_get_connection();
  $query = $query->select('exp_channel_data', 'ecd');
  $query_fields = array();
  $query->addField('ecd', 'entry_id');
  $query->addField('ect', 'title');
  $query->addField('ect', 'channel_id');
  $query->join('exp_channel_titles', 'ect', 'ect.entry_id = ecd.entry_id');
  
  
  foreach ($fields as $field_id => $field) {
    $query->addField('ecd', 'field_id_' . $field_id, $field->field_name);
  }
  
  $query->condition('ecd.channel_id', $channel_id, '=');
  $query->range($page*10, 10);
  $results = $query->execute()
    ->fetchAllAssoc('entry_id');
  
  $rows = array();
  foreach ($results as $entry_id => $entry) {
    $rows[$entry->entry_id] = array(
      'entry_id' => $entry->entry_id,
      'title' => l($entry->title, 'admin/content/ee_analyzer/channels/'.$entry->channel_id.'/entry/' . $entry->entry_id),
    );
  }
  
  //TODO, render this data in a table.
  $header = array(
    array(
      'data' => t('Entry ID'),
      'field' => 'entry_id',
    ),
    array(
      'data' => t('Title'),
      'field' => 'title',
    ),
  );
  
  $render = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  
  $table = drupal_render($render); 
  
  $q = filter_xss($_GET['q']);
  
  $links = l('Previous', $q, array('query' => array('page' => ($page == 0) ? 0 : $page - 1), 'fragment' => 'entries')) . ' ' 
         . l('Next', $q, array('query' => array('page' => $page + 1), 'fragment' => 'entries'));
  
  $table = '<h2 id="entries">Entries</h2>' . $table . $links ;
  
  return $table;
  
}


/**
 * Render a single entry
 */
function ee_analyzer_single_channel_entry($channel_id, $entry_id){
  $query = ee_analyzer_get_connection();
  $field_data = ee_analyzer_get_single_channel_data($query, $channel_id);
      
  $ecd_fields = array('entry_id');
  $ect_fields = array('title');
  
  $query = $query->select('exp_channel_data', 'ecd');
  $query->fields('ecd', $ecd_fields);
  $query->fields('ect', $ect_fields);
  foreach ($field_data as $field_id => $field) {
    $query->addField('ecd', 'field_id_' . $field_id, $field->field_name);
  }
  $query->join('exp_channel_titles', 'ect', 'ect.entry_id = ecd.entry_id');
  $query->condition('ecd.entry_id', $entry_id, '=');
  $result = $query->execute()
    ->fetchAssoc();
  
  drupal_set_title('Entry: '. $result['title']);
  
  $return = '';
  
  foreach ($result as $label => $value) {
    $return .= '<h3>' . $label . '</h3>';
    $return .= '<div><pre>' . htmlentities($value) . '</pre></div>';
  }

  return $return;
}

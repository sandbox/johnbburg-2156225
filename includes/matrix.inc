<?php

/**
 * Display data for a matrix field. 
 */
function ee_analyzer_single_channel_matrix_field($channel_id, $field_id) {

  $matrix = ee_analyzer_single_channel_matrix_field_data_query($field_id);
  
  $header = array(
    array(
      'data' => t('Col ID'),
      'field' => 'field_id',
    ),
    array(
      'data' => t('Col name'),
      'field' => 'field_name',
    ),
    array(
      'data' => t('Col label'),
      'field' => 'field_label',
    ),
    array(
      'data' => t('Col type'),
      'field' => 'field_type',
    ),
  );
  
  $rows = array();
  
  foreach ($matrix as $field) {
    $rows[$field->col_id] = array(
      'col_id' => $field->col_id,
      'field_name' => $field->col_name, 
      'field_label' => $field->col_label,
      'field_type' => $field->col_type,
    );
  }

  $render = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  
  $entries = ee_analyzer_single_channel_matrix_field_data_list($channel_id, $field_id, $matrix);
  
  return drupal_render($render) . $entries;
}


/**
 * Query matrix data
 */
function ee_analyzer_single_channel_matrix_field_data_query($field_id) {
  $query = ee_analyzer_get_connection();
  $query = $query->select('exp_matrix_cols', 'emc');
  $query->fields('emc');
  $query->condition('emc.field_id', $field_id, '=');
  
  $result = $query->execute()
    ->fetchAllAssoc('col_id');
    
  return $result;
}


/**
 * Query entries for matrix fields
 */
function ee_analyzer_query_matrix_entries($channel_id, $field_id, $matrix, $entry_id = NULL) {
  
  if(isset($_GET['page'])){
    $page = filter_xss($_GET['page']);
  }else{
    $page = 0;
  }
  if(!is_numeric($page)){
    $page = 0; 
  }
  
  $query = ee_analyzer_get_connection();
  $query = $query->select('exp_matrix_data', 'emd');
  $query->addField('emd', 'row_id');
  $query->addField('emd', 'entry_id');
  $query->addField('emd', 'row_order');
  
  foreach ($matrix as $col_id => $field) {
    $query->addField('emd', 'col_id_' . $col_id, $field->col_name); 
  }
  
  $query->addField('ect', 'title');
  $query->addField('ect', 'channel_id');
  
  $query->join('exp_channel_titles', 'ect', 'ect.entry_id = emd.entry_id');
  $query->range($page*10, 10);
  
  $query->condition('channel_id', $channel_id, '=');
  
  if($entry_id != NULL) {
    $query->condition('emd.entry_id', $entry_id, '=');
  }
  
  
  $results = $query->execute()
    ->fetchAllAssoc('row_id');
  
  return $results;
}


/**
 * Fetch and render a listing of entry data for matrix fields
 */
function ee_analyzer_single_channel_matrix_field_data_list($channel_id, $field_id, $matrix) {
  
  $results = ee_analyzer_query_matrix_entries($channel_id, $field_id, $matrix);
  
  $rows = array();
  foreach ($results as $row_id => $entry) {
    $rows[$entry->entry_id] = array(
      'entry_id' => $entry->entry_id,
      'title' => l($entry->title, 'admin/content/ee_analyzer/channels/' . $entry->channel_id . '/matrix/' . $field_id . '/' . $entry->entry_id), //TODO, add menu item
    );
  }
  
  $header = array(
    array(
      'data' => t('Entry ID'),
      'field' => 'entry_id',
    ),
    array(
      'data' => t('Title'),
      'field' => 'title',
    ),
  );
  
  $render = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  
  $table = drupal_render($render); 
  
  $q = filter_xss($_GET['q']);
  
  $links = l('Previous', $q, array('query' => array('page' => ($page == 0) ? 0 : $page - 1), 'fragment' => 'entries')) . ' ' 
         . l('Next', $q, array('query' => array('page' => $page + 1), 'fragment' => 'entries'));
  
  $table = '<h2 id="entries">Entries</h2>' . $table . $links ;
  
  return $table;  
}

/**
 * Render the matrix entry data. There will be several matrix entries (rows) for a given Channel entry.
 */
function ee_analyzer_single_channel_matrix_field_entry($channel_id, $field_id, $entry_id){
  $matrix = ee_analyzer_single_channel_matrix_field_data_query($field_id);
  $results = ee_analyzer_query_matrix_entries($channel_id, $field_id, $matrix, $entry_id);
  
  $return = '';
  
  foreach ($results as $row_id => $row) {
    foreach ($row as $label => $value) {
      $return .= '<h3>' . $label . '</h3>';
      $return .= '<div><pre>' . htmlentities($value) . '</pre></div>';
    }
    $return .= '<hr />';
  }
  
  return $return;
}
